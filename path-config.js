import path from 'path'

const name = 'meme-db'
const root = path.join(process.env.APPDATA, name)

export default {
  name,
  paths: {
    root: root,
    images: path.join(root, 'images'),
    db: path.join(root, 'db'),
    temp: path.join(root, 'temp'),
  },
}

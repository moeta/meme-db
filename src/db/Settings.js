import path from 'path'
import fs from 'fs'
import config from '../../path-config.js'

export default class Settings {
  constructor() {
    this._path = path.join(config.paths.root, 'settings.json')
    this.schema = {
      theme: ['dark', 'light'],
    }
  }

  async save() {
    this.write({ theme: this._theme })
  }

  async read() {
    try {
      const data = await fs.promises.readFile(this._path, 'utf8')
      return JSON.parse(data)
    } catch (e) {
      return {}
    }
  }

  async write(data = {}) {
    if (typeof data === 'object' && data !== null) {
      await fs.promises.writeFile(this._path, JSON.stringify(data, null, 2), 'utf8', 'w')
    }
  }

  async update(key, value) {
    const data = await this.read()
    data[key] = value
    this.write(data)
  }

  get theme() {
    return this._theme
  }

  set theme(theme) {
    if (['dark', 'light'].includes(theme)) {
      this._theme = theme
    }
  }
}

import factory from './factory.js'

export default class Tags {
  constructor() {
    this.db = factory('tags')
  }

  async all() {
    return this.db.find({})
  }

  async insert(name) {
    let doc = {
      name: name,
      images: [],
    }
    let tagsWithSameName = await this.db.find({ name: name })
    if (!tagsWithSameName.length) {
      return this.db.insert(doc)
    }
    return null
  }

  async insertMany(names) {
    const inserts = []
    for (let name of names) {
      inserts.push(this.insert(name))
    }
    return Promise.all(inserts)
  }

  async updateIds(removed, remain, id) {
    const newNames = await this.getNewTags(remain)
    await this.insertMany(newNames)
    await this.pushIds(remain, id)
    await this.pullIds(removed, id)
  }

  async removeIds(id) {
    await this.pullId({}, id)
    await this.db.remove({ images: [] })
  }

  async getNewTags(names) {
    const existNames = (await this.db.find({ name: { $in: names } })).map(t => t.name)
    const newNames = names.filter(n => !existNames.includes(n))
    return newNames
  }

  async pullIds(names, id) {
    const promises = []
    for (const name of names) {
      promises.push(this.pullId({ name }), id)
    }
    const tags = await Promise.all(promises)
    this.db.remove({ images: [] })
    return tags
  }

  async pullId(doc, id) {
    return this.db.update(doc, { $pull: { images: id } }, { returnUpdatedDocs: true, multi: true })
  }

  async pushIds(names, id) {
    const promises = []
    for (const name of names) {
      promises.push(this.db.update({ name: `${name}` }, { $push: { images: id } }))
    }
    return Promise.all(promises)
  }

  async findByNames(names) {
    return this.tags.db.find({ name: { $in: names } })
  }
}

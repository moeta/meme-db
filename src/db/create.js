import fs from 'fs'
import config from '../../path-config.js'

export default function create() {
  const dirs = Object.values(config.paths)
  for (let dir of dirs) {
    fs.promises.mkdir(dir, { recursive: true }).catch(console.error)
  }
}

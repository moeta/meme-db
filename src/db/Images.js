import factory from './factory.js'
import Tags from './Tags.js'

export default class Images {
  constructor() {
    this.db = factory('images')
    this.tags = new Tags()
  }

  async all() {
    return this.db.find({}).sort({ createdAt: -1 })
  }

  async find(doc) {
    return this.db.find(doc).sort({ createdAt: -1 })
  }

  async insert(params) {
    const doc = this.standardize(params)
    const image = await this.db.insert(doc)

    this.tags.updateIds([], image.tags, image._id)

    return image
  }

  async update(_id, params = {}) {
    const oldImage = (await this.db.find({ _id }))[0]
    const doc = { $set: { ...params } }

    const { removed, remain } = this.divideTags(oldImage.tags, doc.$set.tags)

    this.tags.updateIds(removed, remain, oldImage._id)
    return this.db.update({ _id }, doc)
  }

  async remove(_id) {
    const remove = await this.db.remove({ _id })
    this.tags.removeIds(_id)
    return remove
  }

  standardize(params = {}) {
    const {
      format = 'jpeg',
      tags = [],
      desc = '',
      width,
      height,
    } = params

    if (tags.length) tags.map((t) => t.trim())
    if (desc) desc.trim()
    if (!Number.isInteger(width + height)) throw Error('Invalid width or height')

    return {
      format,
      tags,
      desc,
      width,
      height,
    }
  }

  divideTags(oldTags, newTags) {
    const allTags = [...new Set([...oldTags, ...newTags])]
    const removed = []
    const remain = []

    for (const tag of allTags) {
      if (oldTags.includes(tag) && !newTags.includes(tag)) {
        removed.push(tag)
      } else {
        remain.push(tag)
      }
    }

    return { removed, remain }
  }
}

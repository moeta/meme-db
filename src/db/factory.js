import Datastore from 'nedb-promises'
import path from 'path'
import config from '../../path-config.js'

export default (name, params = {}) => {
  try {
    return Datastore.create({
      filename: params.filename || path.join(config.paths.db, `${name}.nedb`),
      timestampData: params.timestampData || true,
      autoload: params.autoload || true,
    })
  } catch (e) {
    console.error('error -> ' + e)
    return null
  }
}

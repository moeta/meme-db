import { nativeImage, clipboard } from 'electron'
import https from 'https'
import fs from 'fs'
import path from 'path'
import { settings } from '../db'

import config from '../../path-config.js'

export function pathByName(name, format = 'jpg') {
  if (!name) return null
  return path.join(config.paths.images, `${name}.${format}`)
}

export function copyByPath(p) {
  const img = nativeImage.createFromPath(p)
  clipboard.writeImage(img)
}

export function copyByName(name) {
  return copyByPath(pathByName(name))
}

export async function save(name, format, link) {
  if (link.slice(0, 4) === 'http') {
    return new Promise((resolve) => {
      const file = fs.createWriteStream(pathByName(name))
      https.get(link, response => {
        response.pipe(file)
        resolve()
      })
    })
  }
  return fs.promises.copyFile(link, pathByName(name, format))
}

export function remove(name, format) {
  fs.unlink(pathByName(name, format), err => {
    if (err) console.error(err)
  })
}

export async function themeBackground() {
  const colors = {
    dark: '#272c38',
    light: '#e3e4e6',
  }
  const theme = (await settings.read()).theme
  let color = ''

  if (theme == 'dark') color = colors.dark
  if (theme == 'light') color = colors.light

  return color
}

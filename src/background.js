import {
  app, Menu, Tray, protocol, BrowserWindow,
} from 'electron'
import { createProtocol, installVueDevtools } from 'vue-cli-plugin-electron-builder/lib'
import { themeBackground } from './js/utils.js'
import path from 'path'
import config from '../path-config.js'

const isDevelopment = process.env.NODE_ENV !== 'production'

let win
let tray

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

async function createWindow() {
  win = new BrowserWindow({
    width: 1600,
    height: 900,
    minWidth: 800,
    minHeight: 600,
    backgroundColor: await themeBackground(),
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
    },
  })
  win.removeMenu()
  // win.webContents.openDevTools()
  win.removeMenu()

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    win.loadURL('app://./index.html')
  }

  win.on('closed', () => {
    win = null
  })
}


function toggleVisibility() {
  if (win.isVisible()) {
    win.hide()
  } else {
    win.show()
  }
}

function createTray() {
  tray = new Tray(path.join(__static, 'logo.png'))

  tray.setToolTip(config.name) //
  tray.setTitle(config.name) // macOS
  tray.removeBalloon()

  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show / Hide',
      click() {
        toggleVisibility()
      },
    },
    { type: 'separator' },
    {
      label: 'Exit',
      click() {
        win.close()
      },
    },
  ])
  tray.setContextMenu(contextMenu)

  tray.addListener('click', toggleVisibility)
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    try {
      await installVueDevtools()
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
  createTray()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

import { shallowMount } from '@vue/test-utils'

export let factory = (computed = {}) => {
  return shallowMount(Component, {
    propsData: {},
    mocks: {},
    stubs: {},
    methods: {},
    computed,
  })
}

export let transitionStub = () => ({
  render: function (h) {
    return this.$options._renderChildren
  },
})

export let action = (text, cb) => {
  return describe('action: ' + text, cb)
}

import { mount } from '@vue/test-utils'
import Component from '../src/components/app-settings/app-settings.vue'
import { action } from './utils'
import Vue from 'vue'

let wrapper = mount(Component)

describe('app-settings.vue', () => {
  action('press button close', () => {
    test('should close', async () => {
      let wrapper = mount(Component)
      wrapper.find('.button.close').trigger('click')
      await Vue.nextTick()
      expect(wrapper.contains('.settings')).toBe(true)
    })
  })
})

import { mount } from '@vue/test-utils'
import Component from '../../../src/components/ui/icons/icon-titlebar-minmax.vue'
import { action } from '../../utils'

let wrapper = mount(Component)

describe('icon-titlebar-minmax.vue', () => {
  action('import base icon', () => {
    test('is a Vue instance', () => {
      expect(wrapper.contains('i.icon')).toBe(true)
    })
  })
})

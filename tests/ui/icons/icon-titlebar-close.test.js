import { mount } from '@vue/test-utils'
import Component from '../../../src/components/ui/icons/icon-titlebar-close.vue'
import { action } from '../../utils'

let wrapper = mount(Component)

describe('icon-titlebar-close.vue', () => {
  action('import base icon', () => {
    test('is a Vue instance', () => {
      expect(wrapper.contains('i.icon')).toBe(true)
    })
  })
})

import { shallowMount } from '@vue/test-utils'
import Component from '../src/components/app-gallery/app-gallery.vue'
import { action, transitionStub as transition } from './utils'
import ResizeObserver from './__mocks__/ResizeObserver.js'

let wrapper = shallowMount(Component)

describe('app-gallery', () => {
  action('mounted', () => {
    test('is a Vue instance', () => {
      expect(wrapper.isVueInstance).toBeTruthy()
    })

    test('has gallery class', () => {
      expect(wrapper.contains('.gallery')).toBe(true)
    })
  })

  // test
})

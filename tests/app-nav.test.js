import { shallowMount, mount } from '@vue/test-utils'
import Component from '../src/components/app-nav/app-nav.vue'
import { action, transitionStub } from './utils'

let wrapper = shallowMount(Component)

describe('app-nav.vue', () => {
  action('mounted', () => {
    test('has app-nav class', () => {
      expect(wrapper.contains('.app-nav')).toBe(true)
    })
  })

  action('dark theme choosen', () => {
    test('should render icon theme-dark', async () => {
      let wrapper = mount(Component, { stubs: { transition: transitionStub() } })

      await wrapper.setData({
        theme: 'dark',
      })

      expect(wrapper.find('.icon.dark').vm.$el.style.display).not.toBe('none')
    })
  })

  action('Light theme choosen', () => {
    test('should render icon theme-light', async () => {
      let wrapper = mount(Component, { stubs: { transition: transitionStub() } })

      await wrapper.setData({
        theme: 'light',
      })

      expect(wrapper.find('.icon.light').vm.$el.style.display).not.toBe('none')
    })
  })
})

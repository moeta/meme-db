import { action } from '../utils'
import eventBus from '../../src/js/event-bus'

describe('event-bus.js', () => {
  action('mounted', () => {
    test('is a Vue instance', () => {
      expect(eventBus._isVue).toBe(true)
    })
  })
})

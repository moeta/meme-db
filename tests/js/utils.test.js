import { action } from '../utils'
import * as utils from '../../src/js/utils'

describe('utils.js', () => {
  action('call function pathByName', () => {
    test('no arguments', () => {
      expect(utils.pathByName()).toBe(null)
    })
  })
})

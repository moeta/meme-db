import { shallowMount, mount } from '@vue/test-utils'
import Component from '../../src/components/app-image/image-viewer.vue'
import { action } from '../utils'
import Vue from 'vue'

let wrapper = shallowMount(Component)

describe('image-viewer.vue', () => {
  action('passed prop src', () => {
    test('image should have src value', async () => {
      let wrapper = mount(Component, { props: { src: 'test' } })
      let img = wrapper.find({ ref: 'image' }).element
      img.addEventListener('load', () => {
        expect(img.src).toBe('test')
      })
    })
  })

  // action('passed prop initialDesc', () => {
  //   test('image should have src value', () => {
  //     let wrapper = mount(Component, { props: { initialDesc: 'test' } })
  //     expect(wrapper.find({ref: 'desc'})
  //   })
  // })

  // action('passed prop src', () => {
  //   test('image should have src value', () => {
  //     let wrapper = shallowMount(Component, { props: { src: 'test' } })
  //     expect(wrapper.find({ref: 'tags'})
  //   })
  // })

  // action('clicked on background', () => {
  //   test('should close', () => {
  //     let wrapper = shallowMount(Component, { props: { src: 'test' } })
  //     wrapper.find('.background').trigger('click')
  //   })
  // })


  // test
})

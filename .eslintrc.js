module.exports = {
    'env': {
        'node': true,
        'es6': true,
    },

    extends: [
        'plugin:vue/recommended',
        'airbnb-base/legacy',
    ],

    plugins: [
        'vue',
        'deprecate'
    ],

    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },

    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },

    rules: {
        // vue 
        'vue/html-quotes': ['error','single'],
        'vue/require-default-prop': 'off',
        'vue/name-property-casing': ['warn', 'kebab-case'],
        'vue/no-unused-components': 'warn',
        'vue/require-v-for-key': 'warn',

        // other
        'quotes': ['error','single'],
        'jsx-quotes': ['error','prefer-single'],
        'semi': ['error','never'],
        'indent': ['error',2],

        'comma-dangle': ['warn', 'always-multiline'],
        'linebreak-style': 'off',
        'eqeqeq': ['warn', 'smart'],
        'no-restricted-syntax': 'off',
        'no-await-in-loop': 'off',
        'class-methods-use-this': 'off',
        'no-useless-escape': 'warn',
        'max-len': 'warn',
        'no-underscore-dangle': 'warn',
        'no-plusplus': 'off',
        'no-param-reassign': 'off',
        'no-shadow': 'off',
        'no-undef': 'off',

        'padding-line-between-statements': [
            'warn',
            { blankLine: 'always', prev: ["import"], next: '*'},
            { blankLine: 'any',    prev: ["import"], next: ["import", "const", "let"]}
        ],
    }
}

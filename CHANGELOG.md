# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9.0] - 2020-06-17
### Added
- Vuese comments in code
- Jest tests
- Vue-test-utils tests

### Changed
- Path config
- Artifact now only exe
- Update build options
- Disable dev tools in production
- Disable settings button in navbar

### Fixed
- Image now removed from appdata folder when remove from db
- Theme Toggle icon

## [0.8.0] - 2020-05-13
### Added
- Tagpicker removes tags on backspace

### Fixed
- Tagpicker tag removing
- Tagpicker overflow
- Small fixes

## [0.7.0] - 2020-04-30
### Added
- Add Tray
  - Hide to tray on click button-titlebar-minimize
  - Toggle visibility on tray icon click
  - Toggle visibility in tray menu
  - Exit in tray menu
- Add EventBus
  - Add communication between gallery, image, search
- Add Settings

### Changed
- Update components names to match file names
- Disable some ESLint rules again

### Fixed
- Bug when click tab (circulate) in empty tag picker
- Bug when circulate in tag picker and cursor moving to beginning of text


## [0.6.0] - 2020-04-28
### Added
- Settings db
    - field "theme"
- MainWindow background preloading
- App theme preloading
- Tags db now have field "images" that is array of image ids
- Methods to work with images

### Changed
- Project name from "meme-vue" to "meme-db"
- Database file format from .db to .nedb
- Images db now update Tags db
- Some ESLint rules

### Removed
- uuid module

### Fixed
- Appdata dir creation

## [0.5.0] - 2020-04-27
### Added
- Sidebar
    - Button for toggle search
    - Placeholder button for settings
    - Button for toggle theme
- Search

### Changed
- Rework icons components
- Do small changes that I forgot

## [0.4.0] - 2020-04-22
### Added
- Move to vue

## [0.3.0] - 2020-04-15
### Added
- Image edit UI
- Tag picker
- Image remove UI
- Continuous Integration (CI)

## [0.2.0] - 2020-03-25
### Added
- Gallery
- Upload UI
- Upload via drag & drop
- Upload via paste
- Image copy on click

## [0.1.0] - 2020-02-25
### Added
- Initialize project

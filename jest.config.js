module.exports = {
  // verbose: true,
  moduleFileExtensions: [
    'js',
    'json',
    'vue',
  ],
  transform: {
    '.*\\.(vue)$': 'vue-jest',
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
  },
  reporters: [
    'default',
  ],
  // "testResultsProcessor": "./node_modules/jest-html-reporter",
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{js,vue}',
    '!**/node_modules/**',
  ],
  coverageReporters: ['json', ['html', 'text-summary']],
}
